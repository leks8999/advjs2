// Ответы на теоретические вопросы:
// 1. const text = '{name:"Алексей"}';
// try {
//   const person = JSON.parse(text);
// } catch (error) {
//   console.error(error);
//   console.log(error.message);
// }

// 2. try {
//   town;
// } catch (e) {
//   console.error("Ошибка " + e.message);
// }

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const div = document.getElementById("root");
const newElem = document.createElement("ul");
div.append(newElem);
books.forEach((el) => {
  try {
    if (!el.author || !el.name || !el.price) {
      throw new Error(
        `В книге ${el.name} отсутствует одно или несколько свойств`
      );
    }
    newElem.innerHTML +=
      "<li>" + el.author + ", " + el.name + ", " + el.price + "</li>";
  } catch (e) {
    console.error(e);
  }
});
